.. _acknowledgements:

Acknowledgements
================

Special thanks to Mark Kollasch <https://mastodon.technology/@fool>
for his insight into the 2-pass pattern and
for excellent architecture feedback.

Thanks also to the creators and maintainers of LÖVE,
a wonderful tool which awakened me to the joys of game programming.