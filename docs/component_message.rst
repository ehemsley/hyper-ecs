.. _component_message:

ComponentMessage
================

::

    local ComponentMessage = require("encompass").ComponentMessage

A ComponentMessage is a kind of message that is consumed by
:ref:`ComponentModifiers <component_modifier>` to modify a particular
component.

A ComponentMessage implicitly contains a ``component`` key, but
it can and should be overwritten with a more specific Component subtype.

Function Reference
------------------

.. function:: ComponentMessage.define(name, required_field_types, optional_field_types)

    :param string name: The name of the ComponentMessage prototype.
    :param table required_field_types:
        A table where the keys are field names and the values are types.
        If one of these fields is missing when the ComponentMessage is instantiated,
        or a field is passed to the message with an incorrect type, an error is thrown.
    :param table optional_field_types:
        A table where the keys are field names and the values are types.
        If a field is passed to the message with an incorrect type, an error is thrown.
    :returns: A new ComponentMessage prototype.

**Example**::

    local ComponentMessage = require('encompass').ComponentMessage
    local TransformComponent = require('game.components.transform')

    return ComponentMessage.define('MotionMessage', {
        component = TransformComponent,
        x_velocity = 'number',
        y_velocity = 'number',
        angular_velocity = 'number',
        instant_linear = 'boolean',
        instant_angular = 'boolean'
    })