.. _component_modifier:

ComponentModifier
=================

::

    local ComponentModifier = require('encompass').ComponentModifier

A ComponentModifier is an Engine which is responsible
for consuming :ref:`ComponentMessages <component_message>`
and modifying the referenced :ref:`Component <component>` in response.

ComponentModifiers are defined by the Message prototype they track
and the ``modify`` function they implement.

If a ComponentModifier needs to receive multiple messages that modify
the same component in a single frame,
it should be a :ref:`MultiMessageComponentModifier <multi_message_component_modifier>` instead.

It is an anti-pattern to read Entities or Components inside a ComponentModifier.
*Do not do this.*

Function Reference
------------------

.. function:: ComponentModifier.define(name, message_type, state_message_types)

    :param string name: The name of the ComponentModifier.
    :param prototype message_type: A Message prototype that should be tracked by the ComponentModifier.
    :param table state_message_types: An array-style table of :ref:`StateMessage <state_message>` types that should be tracked by the ComponentModifier. *Optional*

Defines a ComponentModifier that will track the given Message prototype,
and optionally track the given StateMessage types.

.. function:: ComponentModifier:get_state_message(state_message_type)

    :param StateMessage state_message_type: A :ref:`StateMessage <state_message>` prototype that has been tracked by the ComponentModifier.
    :returns: An instantiated StateMessage of the given prototype, or nil if none has been produced this frame.

.. function:: ComponentModifier:modify(component, frozen_fields, message, dt)

    :param Component component: A reference to an instantiated component referenced by the tracked Message.
    :param table frozen_fields:
        A copied table of fields on the referenced component,
        so that the Modifier can read component data without it being affected by other Modifiers.
    :param Message message:
        A reference to a message that has been tracked by the ComponentModifier.
    :param dt:
        The delta time given by the World's current frame update.

This callback is triggered when a Message of the specified prototype is produced by a Detecter.
The programmer must override this callback or an error will be thrown.

Example
-------

::

    local SoundMessage = require('game.messages.sound')
    local TimeDilationStateMessage = require('game.messages.state.time_dilation')

    local ComponentModifier = require('encompass').ComponentModifier
    local SoundModifier = ComponentModifier.define(
        'SoundModifier',
        SoundMessage,
        { TimeDilationStateMessage }
    )

    function SoundModifier:modify(_, frozen_fields, message, dt)
        local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)

        if time_dilation_state_message ~= nil then
            local source = frozen_fields.source
            source:setPitch(time_dilation_state_message.factor)
        end
    end

    return SoundModifier