.. _entity:

Entity
======

::

    local World = require("encompass").World
    local world = World:new()
    local Entity = world:create_entity()

An Entity is composed of a unique internal ID
and a collection of :ref:`Components <component>`.

Entities do not have any implicit properties or behavior,
but are granted these by their collection of Components.

There is no limit to the amount of Components an Entity can have,
and Entities can have any number of Components of a given type.

Entities are `active` by default and can be `deactivated`. While
deactivated, Entities are still tracked by Engines, but the Engines
temporarily ignore them. Note that the activation status of an
Entity does not affect the activation status of its Components.

Defining logic on an Entity is an anti-pattern. *Do not do this.*

List of Functions
-----------------

* :func:`Entity:add_component(component_type, fields) <Entity:add_component>`
* :func:`Entity:get_component(component_type) <Entity:get_component>`
* :func:`Entity:get_components(component_type) <Entity:get_components>`
* :func:`Entity:has_component(component_type) <Entity:has_component>`
* :func:`Entity:remove_component(component) <Entity:remove_component>`
* :func:`Entity:destroy() <Entity:destroy>`
* :func:`Entity:activate() <Entity:activate>`
* :func:`Entity:deactivate() <Entity:deactivate>`

Function Reference
------------------

.. function:: Entity:add_component(component_type, ...)

    :param Component component_type: A Component prototype.
    :param ...:
        Alternating key and value arguments.
        The values should have types appropriate to their matching keys as defined by the Component prototype.
        If these do not match the Message prototype, an error will be thrown.
    :returns: An instantiated Component of the given prototype.

**Example**::

    local World = require('encompass').World
    local Component = require('encompass').Component
    local PositionComponent = Component.define('PositionComponent', {
        x = 'number',
        y = 'number'
    })

    local world = World:new()
    local entity = world:create_entity()
    entity:add_component(PositionComponent,
        'x', 0,
        'y', 0
    )

.. function:: Entity:get_components(component_type)

    :param prototype component_type: A Component prototype.
    :returns: An array-style table containing references to each component of the given Component prototype on the Entity.

.. function:: Entity:get_component(component_type)

    :param prototype component_type: A Component prototype.
    :returns: A reference to an arbitrary first instantiated Component belonging to the object.

This is a convenience method for the common case of having only one
component of a certain type on an Entity.

.. function:: Entity:has_component(component_type)

    :param prototype component_type: A Component prototype.
    :returns: ``boolean`` Whether or not the Entity has a component of the given Component prototype.

.. function:: Entity:remove_component(component)

    :note: An error is thrown if the Entity does not contain the component.
    :param Component component: An instantiated Component.

Removes the given component from the entity.

Runs the `destroy` callback on the component.

.. function:: Entity:destroy()

    :note: The entity is not destroyed until the end of the current frame.

Destroys the Entity and all its components.

.. function:: Entity:activate()

    :note: The entity is not activated until the end of the current frame.

Activates the Entity if it was previously deactivated. Runs ``activate`` callback on all of its components.

.. function:: Entity:deactivate()

    :note: The entity is not deactivated until the end of the current frame.

Deactivates the Entity if it was previously activated. Runs ``deactivate`` callback on all of its components.