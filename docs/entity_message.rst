.. _entity_message:

EntityMessage
=============

::

    local EntityMessage = require("encompass").EntityMessage

An EntityMessage is a kind of message that is consumed by
:ref:`EntityModifiers <entity_modifier>` to modify a particular
entity.

An EntityMessage implicitly contains an ``entity`` key.

Function Reference
------------------

.. function:: EntityMessage.define(name, required_field_types, optional_field_types)

    :param string name: The name of the EntityMessage prototype.
    :param table required_field_types:
        A table where the keys are field names and the values are types.
        If one of these fields is missing when the EntityMessage is instantiated,
        or a field is passed to the message with an incorrect type, an error is thrown.
    :param table optional_field_types:
        A table where the keys are field names and the values are types.
        If a field is passed to the message with an incorrect type, an error is thrown.
    :returns: A new EntityMessage prototype.

**Example**::

    local EntityMessage = require("encompass").EntityMessage
    return EntityMessage.define('DeactivateEntityMessage')