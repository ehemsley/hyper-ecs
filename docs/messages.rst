.. _messages:

Messages
========

Similar to :ref:`Components <component>`, Messages are collections of data.
Message prototypes are defined by their inherited prototype and their
required and optional fields, which are used to instantiate the Messages themselves.
See the :ref:`Field Type Reference <field_type_reference>` for a list of valid field types.

Messages are used to send data to Engines so they can manipulate the game state accordingly.

Unlike Components, Messages are temporary and destroyed at the end of each frame.

For performance reasons, it is discouraged to create new tables to pass to messages,
as this will cause garbage collection issues.

Defining any logic on a Message is an anti-pattern.
Messages should not need to directly trigger any side-effects. *Do not do this.*

.. toctree::
    :maxdepth: 3

    ComponentMessage <component_message>
    EntityMessage <entity_message>
    SpawnMessage <spawn_message>
    StateMessage <state_message>
    SideEffectMessage <side_effect_message>
