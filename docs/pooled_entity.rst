.. _pooled_entity:

PooledEntity
============

A PooledEntity is a special kind of :ref:`Entity <entity>`
which is created by a :ref:`PooledSpawner <pooled_spawner>`.

Instead of creating and destroying Entities at runtime, a PooledSpawner
generates a fixed amount of PooledEntities at initialization time, and then
handles them appropriately when they are deactivated.
This pattern is useful for entities with components
that are expensive to generate at runtime.

PooledEntities are automatically activated by PooledSpawners when
their ``spawn`` callback is called.

PooledEntities should be deactivated instead of destroyed. It is
an error to destroy a PooledEntity.