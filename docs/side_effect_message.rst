.. _side_effect_message:

SideEffectMessage
=================

::

    local SideEffectMessage = require("encompass").SideEffectMessage

A SideEffectMessage is a kind of message that is consumed by
:ref:`SideEffecters <side_effecter>` to modify game state that
isn't captured by any particular Entities or Components. For example,
if your game has multiple top-level states with separate Worlds,
changing the top-level state would be a side effect.

Side effects should be used as an absolute last resort.
Use them only if there is no way to produce the behavior with
other types of Engines.

Function Reference
------------------

.. function:: SideEffectMessage.define(name, required_field_types, optional_field_types)

    :param string name: The name of the SideEffectMessage prototype.
    :param table required_field_types:
        A table where the keys are field names and the values are types.
        If one of these fields is missing when the SideEffectMessage is instantiated,
        or a field is passed to the message with an incorrect type, an error is thrown.
    :param table optional_field_types:
        A table where the keys are field names and the values are types.
        If a field is passed to the message with an incorrect type, an error is thrown.
    :returns: A new SideEffectMessage prototype.