.. _spawn_message:

SpawnMessage
============

::

    local SpawnMessage = require("encompass").SpawnMessage

A SpawnMessage is a kind of message that is consumed by
:ref:`Spawners <spawner>` to spawn new entities.

Function Reference
------------------

.. function:: SpawnMessage.define(name, required_field_types, optional_field_types)

    :param string name: The name of the SpawnMessage prototype.
    :param table required_field_types:
        A table where the keys are field names and the values are types.
        If one of these fields is missing when the SpawnMessage is instantiated,
        or a field is passed to the message with an incorrect type, an error is thrown.
    :param table optional_field_types:
        A table where the keys are field names and the values are types.
        If a field is passed to the message with an incorrect type, an error is thrown.
    :returns: A new SpawnMessage prototype.