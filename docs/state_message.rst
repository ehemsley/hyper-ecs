.. _state_message:

StateMessage
============

::

    local StateMessage = require("encompass").StateMessage

In certain cases, you may want to be able to broadcast information
about the state of the game to multiple Modifiers
without it being consumed by any one Modifiers in particular.
StateMessage exists for this reason.

When a StateMessage is created, it is visible to all Modifiers
that track its prototype.

Defining any logic on a StateMessage is an anti-pattern. *Do not do this.*

Function Reference
------------------

.. function:: StateMessage.define(name, required_field_types, optional_field_types)

    :param string name: The name of the StateMessage prototype.
    :param table required_field_types:
        A table where the keys are field names and the values are types.
        If one of these fields is missing when the StateMessage is instantiated,
        or a field is passed to the message with an incorrect type, an error is thrown.
    :param table optional_field_types:
        A table where the keys are field names and the values are types.
        If a field is passed to the message with an incorrect type, an error is thrown.
    :returns: A new StateMessage prototype.

**Example**

``time_dilation_state_message.lua``
::

    local StateMessage = require('lib.encompass').StateMessage

    return StateMessage.define(
        'TimeDilationStateMessage',
        {
            factor = 'number'
        }
    )

``music_modifier.lua``
::

    local MusicMessage = require('hyperspace.messages.music')
    local TimeDilationStateMessage = require('hyperspace.messages.state.time_dilation')

    local ComponentModifier = require('lib.encompass').ComponentModifier
    local MusicModifier = ComponentModifier.define(
        'MusicModifier',
        MusicMessage,
        { TimeDilationStateMessage }
    )

    function MusicModifier:modify(music_component, frozen_fields, message, dt)
        local time_dilation_state_message = self:get_state_message(TimeDilationStateMessage)

        local factor = 1
        if time_dilation_state_message ~= nil then
            factor = time_dilation_state_message.factor
        end

        dt = dt * factor

        local source = frozen_fields.source
        source:setPitch(factor)
        music_component.time = frozen_fields.time + dt
    end

    return MusicModifier