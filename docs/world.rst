World
=====

::

    local World = require('encompass').World
    local world = World:new()

A World is the glue that holds all the
Engines, Entities, and Components together.

The World's ``update`` function drives the simulation and
can be controlled from your engine's update loop.

It is a mistake to instantiate elements of **encompass**
directly except for World. These elements should be created
either through appropriate World functions, or by Engines.

It is not a good idea to add Engines to the world
at runtime. Set Engines up at initialization time.

List of Functions
-----------------

* :func:`World:new() <World:new>`
* :func:`World:create_entity() <World:create_entity>`
* :func:`World:create_message(message_type) <World:create_message>`
* :func:`World:add_detector(detector_type) <World:add_detector>`
* :func:`World:add_spawner(spawner_type, args) <World:add_spawner>`
* :func:`World:add_modifier(modifier_type, args) <World:add_modifier>`
* :func:`World:add_renderer(renderer_type) <World:add_renderer>`
* :func:`World:destroy_all_entities() <World:destroy_all_entities>`
* :func:`World:update(dt) <World:update>`
* :func:`World:draw(canvas) <World:draw>`

Function Reference
------------------

.. function:: World:new()

    :returns: An instance of World.

.. function:: World:create_entity()

    :returns: An instantiated Entity.

.. function:: World:create_message(message_type, ...)

    :param prototype message_type: A Message prototype.
    :param ...:
        Alternating key and value arguments.
        The values should have types appropriate to their matching keys as defined by the Message prototype.
        If these do not match the Message prototype, an error will be thrown.
    :returns: An instantiated Message of the given prototype.

**Example**::

    world:create_message(MyShipSpawnMessage,
        'x_position', 640,
        'y_position', 360,
        'x_velocity', 0,
        'y_velocity', 0
    )

.. function:: World:add_detector(detector_type)

    :param prototype detector_type: A Detecter prototype.
    :returns: An instantiated Detecter of the given prototype.

.. function:: World:add_spawner(spawner_type, args)

    :param prototype spawner_type: A Spawner prototype
    :param table args: Arguments that will be passed to the Spawner ``initialize`` callback.
    :returns: An instantiated Spawner of the given prototype.

.. function:: World:add_modifier(modifier_type, args)

    :param prototype modifier_type: a Modifier prototype.
    :param table args: Arguments that will be passed to the Modifier ``initailize`` callback.
    :returns: An instantiated Modifier of the given prototype.

.. function:: World:add_renderer(renderer_type)

    :param prototype renderer_type: a Renderer prototype.
    :returns: An instantiated Renderer of the given prototype.

.. function:: World:destroy_all_entities()

Destroys all entities that currently exist in the world. Useful
for situations where you may want to clear the world to reset it
without needing to re-initialize engines.

.. function:: World:update(dt)

    :param number dt: Delta time.

    Updates the simulation based on given delta time, advancing
    the simulation by one frame.

    The actions performed in a frame update are as follows:

    * Activates/Deactivates marked Entities
    * Updates Detectors
    * Registers Messages with appropriate Spawners
    * Updates Spawners
    * Updates Modifiers
    * Destroys marked Entities
    * Recalculates draw order of components if any changed

    Example:
    ::

        function love.update(dt)
            world:update(dt)
        end

.. function:: World:draw(canvas)

    :param Canvas canvas: A reference to a Canvas that all Renderers will be given.


    Draws to the given Canvas_ based on the composition of
    all Renderers in the simulation.

    .. _Canvas: https://love2d.org/wiki/Canvas

    If you're not using LOVE, this could be your engine's concept of a
    Framebuffer or anything you can send draw calls to.

    Example:
    ::

        function love.draw()
            love.graphics.setCanvas(world_canvas)
            love.graphics.clear()
            love.graphics.setCanvas()
            love.graphics.clear()

            world:draw(world_canvas)

            love.graphics.setCanvas()
            love.graphics.setBlendMode('alpha', 'premultiplied')
            love.graphics.setColor(1, 1, 1, 1)
            love.graphics.draw(world_canvas)
        end
