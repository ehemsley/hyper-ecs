#!/bin/bash

START_DIR=$(pwd)

if [ -z "$1" ] 
then
    BUILD_DIR="./build"
else
    BUILD_DIR=$1
fi

# clean build directory
rm -rf $BUILD_DIR
mkdir $BUILD_DIR

# build to build directory
moonc -t $BUILD_DIR ./src
cd $BUILD_DIR

# restructure
mv ./src/* .
rm -rf ./src

# copy lib files to build library folder
cd $START_DIR
cp -r ./lib "$BUILD_DIR/encompass"

# copy library structure files to build folder
cp ./init/encompass.lua $BUILD_DIR
cp ./init/init.lua "$BUILD_DIR/encompass"

echo "Build successful."