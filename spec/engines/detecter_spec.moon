World = require('encompass.world')
Component = require('encompass.component')
Detecter = require('encompass.engines.detecter')
SpawnMessage = require('encompass.messages.spawn')

helper = require('spec.helper')
helper.register(assert)

describe "Detecter", ->
    A = Component\define("A")
    B = Component\define("B")
    C = Component\define("C")

    class TestDetecter extends Detecter
        @component_types: { A, B }
        detect: ->

    describe "check_and_track_entity", ->
        world = World!
        detecter = world\add_detecter TestDetecter

        it "tracks the entity if it contains all required components", ->
            entity_to_track = world\create_entity!
            entity_to_track\add_component A
            entity_to_track\add_component B
            detecter\__check_and_track_entity entity_to_track
            assert.has_value detecter.tracked_entities, entity_to_track
            assert.has_value entity_to_track.tracked_by, detecter

        it "does not track the entity if it does not have all required components", ->
            entity_not_to_track = world\create_entity!
            entity_not_to_track\add_component A
            entity_not_to_track\add_component C
            detecter\__check_and_track_entity entity_not_to_track
            assert.is_not.has_value detecter.tracked_entities, entity_not_to_track
            assert.is_not.has_value entity_not_to_track.tracked_by, detecter

    describe "untrack", ->
        it "untracks the entity", ->
            world = World!
            detecter = world\add_detecter TestDetecter

            entity_to_untrack = world\create_entity!
            entity_to_untrack\add_component A
            entity_to_untrack\add_component B

            detecter\__track_entity entity_to_untrack

            assert.has_value detecter.tracked_entities, entity_to_untrack

            detecter\__untrack_entity entity_to_untrack

            assert.is_not.has_value detecter.tracked_entities, entity_to_untrack
            assert.is_not.has_value entity_to_untrack.tracked_by, detecter

    describe "create message", ->
        it "creates a message and adds it to world", ->
            TestMessage = SpawnMessage\define "TestMessage", a: 'number'
            world = World!
            detecter = world\add_detecter TestDetecter

            message_to_create = detecter\create_message TestMessage, 'a', 3

            assert.has_value world.messages, message_to_create

    describe "when update runs and is tracking an entity", ->
        world = World!

        test_detect_value = 0

        class MyDetecter extends Detecter
            @component_types: { A, B }
            detect: =>
                test_detect_value += 1

        world\add_detecter MyDetecter

        entity = world\create_entity!
        entity\add_component A
        entity\add_component B

        world\update(0.01)

        it "calls the detect callback", ->
            assert.are.equal 1, test_detect_value

    describe "defined with no component types", ->
        it "throws an error", ->
            func = ->
                class MyDetecter extends Detecter
                    detect: ->

            assert.error(func, "MyDetecter cannot have no component types")

    describe "defined with no detect function", ->
        it "throws an error", ->
            func = ->
                class MyDetecter extends Detecter
                    @component_types: { A }

            assert.error(func, "detect must be overridden on MyDetecter")