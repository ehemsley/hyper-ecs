say = require('say')

local helper = {}

function helper.has_value(_, arguments)
    if not type(arguments[1]) == "table" or #arguments ~= 2 then return false end
    for _, v in pairs(arguments[1]) do
        if v == arguments[2] then return true end
    end
    return false
end

function helper.is_empty(_, arguments)
    if not type(arguments[1]) == "table" then return false end
    return next(arguments[1]) == nil
end

say:set("assertion.has_value.positive", "Expected %s \nto have value: %s")
say:set("assertion.has_value.negative", "Expected %s \nto not have value: %s")

say:set("assertion.is_empty.positive", "Expected %s \nto be empty")
say:set("assertion.is_empty.negative", "Expected %s \nnot to be empty")

function helper.register(assert)
    assert:register(
        "assertion",
        "has_value",
        helper.has_value,
        "assertion.has_value.positive",
        "assertion.has_value.negative"
    )

    assert:register(
        "assertion",
        "is_empty",
        helper.is_empty,
        "assertion.is_empty.positive",
        "assertion.is_empty.negative"
    )
end

return helper