World = require "encompass.world"
Component = require "encompass.component"
EntityMessage = require "encompass.messages.entity"
MessagePool = require "encompass.message_pool"

helper = require "spec.helper"
helper.register assert

describe "MessageEntity", ->
    TestEntityMessage = EntityMessage\define "TestEntityMessage"

    describe "when inheriting", ->
        InheritTestEntityMessage = EntityMessage\define "TestEntityMessage", {a: "number"}, {tag: "string"} 
        SubInheritTestEntityMessage = InheritTestEntityMessage\define "SubTestEntityMessage", {x: "number"}, {y: "string"}

        it "properly inherits and defines required field types", ->
            assert.are.same InheritTestComponent, SubInheritTestEntityMessage.required_field_types.component
            assert.are.same "number", SubInheritTestEntityMessage.required_field_types.x
            assert.are.same InheritTestComponent, SubInheritTestEntityMessage.field_types.component
            assert.are.same "number", SubInheritTestEntityMessage.field_types.x

        it "properly inherits and defines optional field types", ->
            assert.are.same "string", SubInheritTestEntityMessage.optional_field_types.tag
            assert.are.same "string", SubInheritTestEntityMessage.optional_field_types.y
            assert.are.same "string", SubInheritTestEntityMessage.field_types.tag
            assert.are.same "string", SubInheritTestEntityMessage.field_types.y

    describe "when checking subtype of", ->
        SubTestEntityMessage = TestEntityMessage\define "SubTestEntityMessage"

        describe "and it is a subtype", ->
            sub_test_entity_message = SubTestEntityMessage!

            it "returns true", ->
                assert.true sub_test_entity_message\subtype_of SubTestEntityMessage
                assert.true sub_test_entity_message\subtype_of EntityMessage

        describe "and it is not a subtype", ->
            SubEntityMessage = EntityMessage\define "SubEntityMessage"
            sub_entity_message = SubEntityMessage!

            it "returns false", ->
                assert.false sub_entity_message\subtype_of SubTestEntityMessage

    describe "when deactivating", ->
        world = World!
        entity = world\create_entity!
        message = world\create_message TestEntityMessage, "entity", entity
        message_pool = world.message_type_to_pool[TestEntityMessage]
        free = spy.on message_pool, "free"

        it "frees the message from its message pool", ->
            message\__deactivate!
            assert.spy(message_pool.free).was_called!
            assert.has_value message_pool.inactive_objects, message
            assert.not.has_value message_pool.active_objects, message
