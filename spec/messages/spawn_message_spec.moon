World = require "encompass.world"
Component = require "encompass.component"
SpawnMessage = require "encompass.messages.spawn"
MessagePool = require "encompass.message_pool"

helper = require "spec.helper"
helper.register assert

describe "MessageEntity", ->
    TestSpawnMessage = SpawnMessage\define "TestSpawnMessage"

    describe "when inheriting", ->
        InheritTestSpawnMessage = SpawnMessage\define "TestSpawnMessage", {a: "number"}, {tag: "string"} 
        SubInheritTestSpawnMessage = InheritTestSpawnMessage\define "SubTestSpawnMessage", {x: "number"}, {y: "string"}

        it "properly inherits and defines required field types", ->
            assert.are.same InheritTestComponent, SubInheritTestSpawnMessage.required_field_types.component
            assert.are.same "number", SubInheritTestSpawnMessage.required_field_types.x
            assert.are.same InheritTestComponent, SubInheritTestSpawnMessage.field_types.component
            assert.are.same "number", SubInheritTestSpawnMessage.field_types.x

        it "properly inherits and defines optional field types", ->
            assert.are.same "string", SubInheritTestSpawnMessage.optional_field_types.tag
            assert.are.same "string", SubInheritTestSpawnMessage.optional_field_types.y
            assert.are.same "string", SubInheritTestSpawnMessage.field_types.tag
            assert.are.same "string", SubInheritTestSpawnMessage.field_types.y

    describe "when checking subtype of", ->
        SubTestSpawnMessage = TestSpawnMessage\define "SubTestSpawnMessage"

        describe "and it is a subtype", ->
            sub_test_spawn_message = SubTestSpawnMessage!

            it "returns true", ->
                assert.true sub_test_spawn_message\subtype_of SubTestSpawnMessage
                assert.true sub_test_spawn_message\subtype_of SpawnMessage

        describe "and it is not a subtype", ->
            SubSpawnMessage = SpawnMessage\define "SubSpawnMessage"
            sub_spawn_message = SubSpawnMessage!

            it "returns false", ->
                assert.false sub_spawn_message\subtype_of SubTestSpawnMessage

    describe "when deactivating", ->
        world = World!
        entity = world\create_entity!
        message = world\create_message TestSpawnMessage
        message_pool = world.message_type_to_pool[TestSpawnMessage]
        free = spy.on message_pool, "free"

        it "frees the message from its message pool", ->
            message\__deactivate!
            assert.spy(message_pool.free).was_called!
            assert.has_value message_pool.inactive_objects, message
            assert.not.has_value message_pool.active_objects, message
