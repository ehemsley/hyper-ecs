World = require('encompass.world')
Component = require('encompass.component')
ComponentMessage = require('encompass.messages.component')
Detecter = require('encompass.engines.detecter')
PooledSpawner = require('encompass.engines.processors.pooled_spawner')

helper = require('spec.helper')
helper.register assert

describe "PooledEntity", ->
    MockComponent = Component\define "MockComponent", {}, a: "number"
    MockComponentB = Component\define "MockComponentB", x: "number", y: "number"

    describe "when created", ->
        world = World!
        pooled_entity = world\__create_pooled_entity!
        component = pooled_entity\add_component MockComponent

        it "is immediately deactivated", ->
            world\update 0.01
            assert.false pooled_entity.active
            assert.false component.__active

    describe "when adding component", ->
        world = World!
        pooled_entity = world\__create_pooled_entity!
        component = pooled_entity\add_component MockComponent

        describe "argument is nil", ->
            it "throws an error", ->
                nil_component = nil
                error_entity = world\__create_pooled_entity!
                func = -> error_entity\add_component nil_component

                assert.error func, "Component is nil. Did you forget to require?"

        describe "and argument is not a Component", ->
            TestMessage = ComponentMessage\define "TestMessage"

            func = -> pooled_entity\add_component TestMessage

            it "throws an error", ->
                assert.error func, "Attempted to add a non Component to an Entity"

        describe "and argument is valid", ->
            pooled_entity = world\__create_pooled_entity!
            component = pooled_entity\add_component MockComponent

            it "adds the component to the map", ->
                assert.has_value pooled_entity.component_map[MockComponent], component

            it "adds the component type to the component type set", ->
                assert.has_value pooled_entity.component_types[MockComponent], MockComponent

        describe "and field with incorrect type passed in", ->
            func = ->
                pooled_entity\add_component MockComponentB, "x", "hello", "y", 20

            it "throws an error", ->
                assert.error func, "value for 'x' on MockComponentB should be of type number"

        describe "and there is a missing required field", ->
            func = ->
                pooled_entity\add_component MockComponentB, "y", 20

            it "throws an error", ->
                assert.error func, "missing field 'x' on MockComponentB"

        describe "and there are optional fields", ->
            describe "and an optional field is passed in", ->
                it "assigns the value", ->
                    mock_component = pooled_entity\add_component MockComponent, "a", 23
                    assert.are.equal 23, mock_component.a

            describe "missing optional field", ->
                func = -> pooled_entity\add_component MockComponent

                it "does not care", ->
                    assert.has.no.error func

        describe "and there are multiples of the component", ->
            it "does not throw an error", ->
                multiples_entity = world\__create_pooled_entity!
                MultipleTestComponent = Component\define "MultipleTestComponent"

                func = ->
                    multiples_entity\add_component MultipleTestComponent
                    multiples_entity\add_component MultipleTestComponent

                assert.has.no.error func

        describe "and an undefined field is passed in ", ->
            func = ->
                pooled_entity\add_component MockComponentB, "x", 10, "z", 30

            it "throws an error", ->
                assert.error func, "field 'z' is not defined on MockComponentB"

    describe "when removing component", ->
        class TestDetecter extends Detecter
            @component_types: { MockComponent }
            detect: ->

        world = World!
        test_detecter = world\add_detecter TestDetecter

        describe "after removal but before update", ->
            it "adds itself to world list of entities with removed components", ->
                entity_to_check = world\__create_pooled_entity!
                component_to_check = entity_to_check\add_component MockComponent
                world\__check_entities_with_added_components!
                entity_to_check\remove_component component_to_check
                assert.has_value world.entities_with_removed_components, entity_to_check

        describe "after removal", ->
            on_destroy = spy.new(->)

            class MockDestroyComponent extends Component
                on_destroy: on_destroy

            pooled_entity = world\__create_pooled_entity!
            component = pooled_entity\add_component MockDestroyComponent

            world\__check_entities_with_added_components!
            pooled_entity\remove_component component
            world\__check_entities_with_removed_components!

            it "removes all references to the component", ->
                assert.is_empty pooled_entity.component_map[MockDestroyComponent]
                assert.is_empty pooled_entity.component_types[MockDestroyComponent]

            it "untracks from relevant engines", ->
                assert.not.has_value pooled_entity.tracked_by, test_detecter
                assert.not.has_value test_detecter.tracked_entities, pooled_entity

            it "runs the destroy callback on the component", ->
                assert.spy(on_destroy).was_called!

        describe "and entity does not have component", ->
            entity = world\__create_pooled_entity!
            entity_id = entity.id
            entity_two = world\__create_pooled_entity!

            entity\add_component MockComponent
            component_two = entity_two\add_component MockComponent

            world\__check_entities_with_added_components!

            func = -> entity\remove_component component_two

            it "throws an error", ->
                assert.has.error func, "Entity " .. entity_id .. " does not have component instance of MockComponent"

    describe "when getting component", ->
        world = World!

        describe "and entity has multiple components of that type", ->
            it "returns an arbitrary component instance", ->
                entity = world\__create_pooled_entity!
                component = entity\add_component MockComponent
                component_two = entity\add_component MockComponent

                result = entity\get_component MockComponent
                assert.true result == component or result == component_two

        describe "and entity only has one component of that type", ->
            it "returns the component instance", ->
                entity = world\__create_pooled_entity!
                component = entity\add_component MockComponent

                assert.are.same entity\get_component(MockComponent), component

        describe "and entity has no components of that type", ->
            entity = world\__create_pooled_entity!
            entity_id = entity.id
            func = -> entity\get_component MockComponent

            it "throws an error", ->
                assert.error func, "Entity " .. entity_id .. " does not have component of type MockComponent"

    describe "when getting components", ->
        world = World!
        MultipleTestComponent = Component\define "MultipleTestComponent", var: "number"

        describe "and entity has multiple components", ->
            entity = world\create_entity!
            first = entity\add_component MultipleTestComponent, "var", 1
            second = entity\add_component MultipleTestComponent, "var", 2

            it "returns all the components of that type", ->
                result = entity\get_components MultipleTestComponent
                assert.has_value result, first
                assert.has_value result, second

        describe "and entity has no components of that type", ->
            entity = world\__create_pooled_entity!
            entity_id = entity.id

            func = -> entity\get_components MockComponent

            it "throws an error", ->
                assert.has.error func, "Entity " .. entity_id .. " does not have component of type MockComponent"

    describe "when checking if has a component of a certain type", ->
        describe "and the component has an entity of that type", ->
            world = World!
            entity = world\__create_pooled_entity!
            entity\add_component MockComponent

            it "returns true", ->
                assert.true entity\has_component MockComponent

        describe "and the component never had an entity of that type", ->
            world = World!
            entity = world\__create_pooled_entity!

            it "returns false", ->
                assert.false entity\has_component MockComponent

        describe "and the component was removed from the entity", ->
            world = World!
            entity = world\__create_pooled_entity!
            component = entity\add_component MockComponent

            world\__check_entities_with_added_components!
            entity\remove_component component
            world\__check_entities_with_removed_components!

            it "return false", ->
                assert.false entity\has_component MockComponent

    describe "when destroy is called", ->
        deactivate = spy.new(->)
        class MockPooledSpawner
            __deactivate: deactivate

        world = World!
        entity = world\__create_pooled_entity MockPooledSpawner!
        entity\destroy!

        it "deactivates the pooled entity", ->
            assert.spy(deactivate).was_called!

    describe "deactivate", ->
        on_deactivate = spy.new(->)
        detect = spy.new(->)

        class TestComponent extends MockComponent
            on_deactivate: on_deactivate

        class TestDetecter extends Detecter
            @component_types: { TestComponent }
            detect: detect

        world = World!
        world\add_detecter TestDetecter

        entity = world\__create_pooled_entity!
        entity\add_component TestComponent
        entity\add_component TestComponent
        world\__check_entities_with_added_components!
        entity\__deactivate!
        world\update 0.01

        it "deactivates the entity", ->
            assert.false entity.active

        it "does not get detected", ->
            assert.spy(detect).was_not_called

        it "performs the deactivation callbacks on each component", ->
            assert.spy(on_deactivate).was_called(2)

    describe "activate", ->
        on_activate = spy.new(->)
        detect = spy.new(->)

        class TestComponent extends MockComponent
            on_activate: on_activate

        class TestDetecter extends Detecter
            @component_types: { TestComponent }
            detect: detect

        world = World!
        world\add_detecter TestDetecter

        entity = world\__create_pooled_entity!
        entity\add_component TestComponent
        entity\add_component TestComponent
        entity\__deactivate!
        world\__check_entities_with_added_components!
        entity\__activate!
        world\__detect 0.01

        it "activates the entity", ->
            assert.true entity.active

        it "is detected", ->
            assert.spy(detect).was_called

        it "performs activation callback on each component", ->
            assert.spy(on_activate).was_called(2)