World = require "encompass.world"
Component = require "encompass.component"
DrawComponent = require "encompass.draw_component"

ComponentMessage = require "encompass.messages.component"
SideEffectMessage = require "encompass.messages.side_effect"
SpawnMessage = require "encompass.messages.spawn"

Detecter = require "encompass.engines.detecter"
Renderer = require "encompass.engines.renderer"
Spawner = require "encompass.engines.processors.spawner"
SideEffecter = require "encompass.engines.processors.side_effecter"
ComponentModifier = require "encompass.engines.processors.modifiers.component"

helper = require "spec.helper"
helper.register assert

describe "World", ->
    describe "when getting next id", ->
        it "should start at 1", ->
            world = World!
            assert.are.equal 1, world\__next_id!
        
        it "should increment by 1 every time it is called", ->
            world = World!
            world\__next_id!
            world\__next_id!
            assert.are.equal 3, world\__next_id!

    describe "when creating entity", ->
        it "should assign an id", ->
            world = World!
            world\__next_id!

            entity = world\create_entity!
            assert.are.equal 2, entity.id

        it "should assign world", ->
            world = World!
            entity = world\create_entity!
            assert.are.same world, entity.world

        it "should create a reference to the entity on the world", ->
            world = World!
            entity = world\create_entity!

            assert.are.equal entity, world.entities[entity]

        it "should create a reference to the entity by id", ->
            world = World!
            entity = world\create_entity!

            assert.are.equal entity, world.id_to_entity[entity.id]

    describe "when creating component", ->
        TestComponent = Component\define "TestComponent"
        world = World!
        entity = world\create_entity!
        component = world\__create_component TestComponent, entity

        it "assigns the entity to the component", ->
            assert.are.equal component.__entity, entity
        
        it "creates a component of the given type", ->
            assert.are.same component.__class, TestComponent

    describe "when adding detecter", ->
        TestComponentA = Component\define "TestComponentA"
        TestComponentB = Component\define "TestComponentB"
        class TestDetecter extends Detecter
            @component_types: { TestComponentA, TestComponentB }
            detect: ->

        world = World!
        detecter = world\add_detecter TestDetecter

        it "adds a reference to the detecter on the world", ->
            assert.has_value world.detecters, detecter

        it "adds the detecter to the component to engine map", ->
            assert.has_value world.component_type_to_engine_map[TestComponentA], detecter
            assert.has_value world.component_type_to_engine_map[TestComponentB], detecter

    describe "when adding renderer", ->
        TestDrawComponent = DrawComponent\define "TestDrawComponent"
        class TestRenderer extends Renderer
            @component_types: { TestDrawComponent }
            render: ->

        world = World!
        renderer = world\add_renderer TestRenderer

        it "adds a reference to the renderer on the world", ->
            assert.has_value world.renderers, renderer

        it "adds the renderer to the component to engine map", ->
            assert.has_value world.component_type_to_engine_map[TestDrawComponent], renderer
        
        it "adds the renderer to the draw component to renderer map", ->
            assert.are.same world.draw_component_type_to_renderer[TestDrawComponent], renderer

    describe "when adding component modifier", ->
        TestComponentMessage = ComponentMessage\define "TestComponentMessage"

        describe "without arguments", ->
            world = World!
            class TestComponentModifier extends ComponentModifier
                @message_type: TestComponentMessage
                modify: ->

            component_modifier = world\add_modifier TestComponentModifier

            it "creates processor", ->
                assert.has_value world.modifiers, component_modifier

            it "adds the modifier to the message to processor map", ->
                assert.are.same world.message_type_to_processor_map[TestComponentMessage], component_modifier

        describe "with arguments", ->
            world = World!
            class TestComponentModifier extends ComponentModifier
                @message_type: TestComponentMessage
                modify: ->

                initialize: (thing) =>
                    @thing = thing

            component_modifier = world\add_modifier TestComponentModifier, 5

            it "creates processor and initializes with arguments", ->
                assert.has_value world.modifiers, component_modifier
                assert.are.same 5, component_modifier.thing

            it "adds the modifier to the message to processor map", ->
                assert.are.same world.message_type_to_processor_map[TestComponentMessage], component_modifier
        
    describe "detect", ->
        TestComponentA = Component\define "TestComponentA"
        TestComponentB = Component\define "TestComponentB"
        
        detectA = spy.new(->)
        detectB = spy.new(->)

        class TestDetecterA extends Detecter
            @component_types: { TestComponentA }
            detect: detectA

        class TestDetecterB extends Detecter
            @component_types: { TestComponentB }
            detect: detectB

        world = World!
        world\add_detecter TestDetecterA
        world\add_detecter TestDetecterB

        entity = world\create_entity!
        entity\add_component TestComponentA
        entity\add_component TestComponentB
        
        world\__check_entities_with_added_components!

        it "tells the detecters to detect", ->
            world\__detect 0.01
            assert.spy(detectA).was_called!
            assert.spy(detectB).was_called!

    describe "process", ->
        TestSpawnMessage = SpawnMessage\define "TestSpawnMessage", count: "number"
        TestSideEffectMessage = SideEffectMessage\define "TestSideEffectMessage"

        effect = spy.new(->)
        spawn = spy.new(->)

        class TestSideEffecter extends SideEffecter
            @message_type: TestSideEffectMessage
            effect: effect

        class TestSpawner extends Spawner
            @message_type: TestSpawnMessage
            spawn: spawn

        world = World!
        side_effecter = world\add_side_effecter TestSideEffecter
        spawner = world\add_spawner TestSpawner

        side_effect_message = world\create_message TestSideEffectMessage
        spawn_message = world\create_message TestSpawnMessage, "count", 4
        world\update 0.01

        it "tells the processors to process", ->
            assert.spy(effect).was_called!
            assert.spy(spawn).was_called!

        it "removes message after processing", ->
            assert.not.has_value world.messages, spawn_message
            assert.not.has_value world.messages, side_effect_message

        it "removes message from processor after processing", ->
            assert.not.has_value spawner.messages, spawn_message
            assert.not.has_value side_effecter.messages, side_effect_message

    describe "checking and tracking entities", ->
        TestComponentA = Component\define "TestComponentA"
        TestComponentB = Component\define "TestComponentB"

        check_and_track_a = spy.new(->)
        check_and_track_b = spy.new(->)

        class TestDetecterA extends Detecter
            @component_types: { TestComponentA }
            detect: ->
            __check_and_track_entity: check_and_track_a

        class TestDetecterB extends Detecter
            @component_types: { TestComponentB }
            detect: ->
            __check_and_track_entity: check_and_track_b

        world = World!
        detecter_a = world\add_detecter TestDetecterA
        detecter_b = world\add_detecter TestDetecterB

        entity = world\create_entity!
        entity\add_component TestComponentA

        world\update 0.01

        it "calls the method on relevant detecters", ->
            assert.spy(check_and_track_a).was_called!
            assert.spy(check_and_track_b).was_not_called!

    describe "checking and tracking messages", ->
        TestComponentA = Component\define "TestComponentA"
        TestComponentB = Component\define "TestComponentB"

        TestMessageA = ComponentMessage\define "TestMessageA", component: TestComponentA
        TestMessageB = ComponentMessage\define "TestMessageB", component: TestComponentB

        check_and_track_a = spy.new(->)
        check_and_track_b = spy.new(->)

        class TestComponentModifierA extends ComponentModifier
            message_type: TestMessageA
            modify: ->
            check_and_track_message: check_and_track_a

        class TestComponentModifierB extends ComponentModifier
            message_type: TestMessageB
            modify: ->
            check_and_track_message: check_and_track_b

        world = World!
        world\add_modifier TestComponentModifierA
        world\add_modifier TestComponentModifierB

        entity = world\create_entity!
        test_component_b = entity\add_component TestComponentB

        world\create_message TestMessageB, "component", test_component_b
        world\update 0.01

        it "calls the check_and_track_message method on relevant processors", ->
            assert.spy(check_and_track_a).was_not_called!
            assert.spy(check_and_track_b).was_called!

    describe "find entity", ->
        world = World!
        entity = world\create_entity!
        entity_id = entity.id

        it "finds entity by id", ->
            assert.are.same entity, world\find_entity(entity_id)

    describe "destroying all marked entities", ->
        world = World!
        entity = world\create_entity!
        entity_two = world\create_entity!

        entity\destroy! --marks for destroy
        entity_two\destroy!

        destroy = spy.on world, "__destroy_entity"
        world\__destroy_marked_entities!
        
        it "calls __destroy_entity on the marked entities", ->
            assert.spy(destroy).was_called 2

    describe "destroying an entity", ->
        on_destroy = spy.new(->)

        class TestComponent extends Component
            on_destroy: on_destroy

        class TestDetecter extends Detecter
            component_types: { TestComponent }
            detect: ->

        world = World!
        entity = world\create_entity!
        detecter = world\add_detecter TestDetecter

        component = entity\add_component TestComponent

        world\__destroy_entity entity

        it "removes entity reference from world", ->
            assert.not.has_value world.entities, entity

        it "removes entity from relevant detecters", ->
            assert.not.has_value detecter.tracked_entities, entity

        it "removes references to detecters", ->
            assert.not.has_value entity.tracked_by, detecter

        it "calls the destroy callback on components", ->
            assert.spy(on_destroy).was_called!

    describe "destroying all entities", ->
        class TestComponent extends Component
        
        class TestDetecter extends Detecter
            component_types: { TestComponent }
            detect: ->

        world = World!
        test_detecter = world\add_detecter TestDetecter

        entity_one = world\create_entity!
        entity_one\add_component TestComponent

        entity_two = world\create_entity!
        entity_two\add_component TestComponent

        world\update 0.01
        world\destroy_all_entities!

        it "destroys all entities", ->
            assert.nil next(world.entities)

        it "untracks all entities from detecters", ->
            assert.nil next(test_detecter.tracked_entities)

        it "does not destroy engines", ->
            assert.has_value world.detecters, test_detecter

    describe "drawing", ->
        world = World!
        class ADrawComponent extends DrawComponent
        class BDrawComponent extends DrawComponent
        class CDrawComponent extends DrawComponent

        draw_order = {}
        class ARenderer extends Renderer
            component_types: { ADrawComponent }
            render: -> table.insert draw_order, ADrawComponent

        class BRenderer extends Renderer
            component_types: { BDrawComponent }
            render: -> table.insert draw_order, BDrawComponent

        class CRenderer extends Renderer
            component_types: { CDrawComponent }
            render: -> table.insert draw_order, CDrawComponent

        world\add_renderer ARenderer
        world\add_renderer BRenderer
        world\add_renderer CRenderer

        entity = world\create_entity!
        test_draw_component = entity\add_component ADrawComponent, "layer", 1

        entity_two = world\create_entity!
        entity_two\add_component ADrawComponent, "layer", 3
        entity_two\add_component BDrawComponent, "layer", 2

        entity_three = world\create_entity!
        entity_three\add_component CDrawComponent, "layer", 4

        world\update 0.01
        world\draw!

        it "draws components with correct renderer methods in correct order", ->
            assert.are.same {ADrawComponent, BDrawComponent, ADrawComponent, CDrawComponent}, draw_order

        describe "when a draw component layer is updated", ->
            test_draw_component.layer = 5
            draw_order = {}
            world\update 0.01
            world\draw!

            it "draws components in correct order", ->
                assert.are.same {BDrawComponent, ADrawComponent, CDrawComponent, ADrawComponent}, draw_order