_prefix = (...)\match("(.+%.)[^%.]+$") or ""

{ :getters, :setters } = require(_prefix .. 'object_ext')
Component = require(_prefix .. 'component')

class DrawComponent extends Component
    @required_field_types: { layer: "number" }

    @__inherited: (subklass) =>
        getters subklass,
            layer: =>
                rawget @, "__layer"

        setters subklass,
            layer: (value) =>
                if rawget @, '__layer' == value then return
                @__entity.world\__adjust_component_draw_layers(@, rawget(self, '__layer'), value)
                rawset @, '__layer', value

        Component.__inherited @, subklass

return DrawComponent