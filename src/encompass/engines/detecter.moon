up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_one_folder .. 'utils')
Engine = require(up_one_folder .. 'engine')

class Detecter extends Engine
    @__inherited: (subklass) =>
        subklass\assert_component_types!
        assert subklass.detect != nil and utils.callable(subklass.detect), "detect must be overridden on " .. subklass.__name

    __update: =>
        for _, entity in pairs @tracked_entities
            @detect entity

    create_message: (message, ...) =>
        @world\create_message message, ...

return Detecter