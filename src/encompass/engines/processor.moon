up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_one_folder .. 'utils')

class Processor
    @state_message_types: {}

    @assert_has_message_type: =>
        assert @@message_type != nil, @@__name .. " must have a message_type property"

    new: (world, ...) =>
        @messages = {}
        @state_messages = {}
        @world = world

        @initialize ...

    initialize: ->

    subtype_of: (klass) =>
        return @@ == klass or (@@.__parent ~= nil and @@.__parent\subtype_of klass)

    get_state_message: (state_message_type) =>
        @state_messages[state_message_type]

    create_entity: =>
        @world\create_entity!

    __check_message: (message) =>
        message.__class == @@message_type

    __track_message: (message) =>
        table.insert @messages, message

    check_and_track_message: (message) =>
        if @__check_message message then
            @__track_message message

    __check_and_track_state_message: (state_message) =>
        for _, state_message_type in pairs @@state_message_types
            if state_message.__class == state_message_type then
                @state_messages[state_message.__class] = state_message

    __clean: =>
        utils.clear @messages
        utils.clear @state_messages

return Processor