up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

Processor = require(up_one_folder .. 'processor')

class Modifier extends Processor
    new: (world, ...) =>
        @component_type_to_frozen_field_table = {}
        super world, ...

    __create_frozen_fields: (component) =>
        if @component_type_to_frozen_field_table[component.__class] == nil then
            @component_type_to_frozen_field_table[component.__class] = {}

        frozen_fields = @component_type_to_frozen_field_table[component.__class]
        for k, _ in pairs component.__class.field_types
            frozen_fields[k] = component[k]
        frozen_fields

return Modifier