up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'
up_three_folders = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_three_folders .. 'utils')
EntityMessage = require(up_three_folders .. 'messages.entity')
Modifier = require(up_one_folder .. 'modifier')

class EntityModifier extends Modifier
    @__inherited: (subklass) =>
        subklass\assert_has_message_type!
        assert subklass.message_type\subtype_of(EntityMessage), subklass.__name .. " must consume a message of EntityMessage type"
        assert subklass.modify != nil and utils.callable(subklass.modify), subklass.__name .. " must implement modify function"

    new: (world, ...) =>
        @entity_to_messages = {}
        super world, ...

    __update: (dt) =>
        for entity, messages in pairs @entity_to_messages
            if #messages > 0 then
                @modify entity, messages, dt

    __track_message: (message) =>
        if @entity_to_messages[message.entity] == nil then
            @entity_to_messages[message.entity] = {}

        table.insert @entity_to_messages[message.entity], message

    __untrack_message: (message) =>
        utils.remove @entity_to_messages[message.entity], message

    __clean: =>
        utils.clear @state_messages
        for _, t in pairs @entity_to_messages
            utils.clear t

return EntityModifier