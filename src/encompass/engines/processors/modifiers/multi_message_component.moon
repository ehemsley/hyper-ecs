up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'
up_three_folders = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

utils = require(up_three_folders .. 'utils')
ComponentMessage = require(up_three_folders .. 'messages.component')
Modifier = require(up_one_folder .. 'modifier')

class MultiMessageComponentModifier extends Modifier
    @__inherited: (subklass) =>
        subklass\assert_has_message_type!
        assert subklass.message_type\subtype_of(ComponentMessage), subklass.__name .. " must consume messages of ComponentMessage type"
        assert subklass.modify != nil and utils.callable(subklass.modify), subklass.__name .. " must implement modify function"

    new: (world, ...) =>
        @component_to_messages = {}
        super world, ...

    __update: (dt) =>
        for component, messages in pairs @component_to_messages
            if #messages > 0 then
                @modify component, @__create_frozen_fields(component), messages, dt

    __track_message: (message) =>
        if @component_to_messages[message.component] == nil then
            @component_to_messages[message.component] = {}

        table.insert self.component_to_messages[message.component], message

    __untrack_message: (message) =>
        utils.remove @component_to_messages[message.component], message

    __clean: =>
        utils.clear @state_messages
        for _, t in pairs @component_to_messages
            utils.clear t