current_folder = (...)\gsub('%.[^%.]+$', '') .. '.'

utils = require(current_folder .. 'utils')
Message = require(current_folder .. 'message')
DrawComponent = require(current_folder .. 'draw_component')

class Entity
    new: (id, world) =>
        @id = id
        @world = world

        @active = true

        @marked_for_activation = false
        @marked_for_deactivation = false

        @component_map = {}
        @component_types = {}

        @components = {}
        @draw_components = {}
        @tracked_by = {}

    process_component_field_args = (component_type, component, key, value, ...) ->
        if key == nil then return

        -- check that the field is defined in the prototype
        assert component_type.field_types[key] ~= nil,
            "field '" ..
            key ..
            "' is not defined on " ..
            component_type.__name

        -- check that the given value type is correct as defined in the prototype
        if value ~= nil then -- if value is nil, it may be optional and we'll check it later
            assert utils.encompasstype(value, component_type.field_types[key]),
                "value for '" ..
                key ..
                "' on " ..
                component_type.__name ..
                " should be of type " ..
                tostring(component_type.field_types[key])

            component[key] = value

        process_component_field_args(component_type, component, ...)

    register_component: (component_type, key, value, ...) =>
        assert component_type ~= nil,
            "SuperComponent is nil. Did you forget to require?"

        component = @world\__create_component component_type, @

        if not utils.empty component_type.required_field_types then
            if key == nil or value == nil then
                error "Missing field arguments on " .. component_type.__name


        process_component_field_args(component_type, component, key, value, ...)

        for k, _ in pairs component_type.required_field_types do
            assert component[k] ~= nil,
                    "missing field '" ..
                    k ..
                    "' on " ..
                    component_type.__name

        if @component_map[component_type] == nil then
            @component_map[component_type] = {}
        table.insert @component_map[component_type], component

        if @component_types[component_type] == nil then
            @component_types[component_type] = {}
        table.insert @component_types[component_type], component_type

        @components[component] = component

        component\on_initialize()

        component

    add_component: (component_type, ...) =>
        if component_type == nil then
            error "Component is nil. Did you forget to require?"

        if component_type\subtype_of Message then
            error "Attempted to add a non Component to an Entity"

        component = @register_component component_type, ...
        table.insert @world.entities_with_added_components, @

        if component_type\subtype_of DrawComponent then
            @draw_components[component] = component

        component

    remove_component: (component) =>
        assert @components[component] ~= nil,
            "Entity " ..
            @id ..
            " does not have component " ..
            tostring(component)

        component_type = component.__class

        @components[component] = nil

        if component\subtype_of DrawComponent then
            @draw_components[component] = nil
            @world\__remove_draw_component(component)

        utils.remove(@component_map[component_type], component)
        utils.remove(@component_types[component_type], component_type)

        component\on_destroy()

        table.insert @world.entities_with_removed_components, @

    get_component: (component_type) =>
        if not @has_component component_type then
            error "Entity " .. @id .. " does not have component of type " .. component_type.__name
        @component_map[component_type][1]

    get_components: (component_type) =>
        if not @has_component component_type then
            error "Entity " .. @id .. " does not have component of type " .. component_type.__name
        @component_map[component_type]

    has_component: (component_type) =>
        @component_types[component_type] ~= nil and #@component_types[component_type] ~= 0

    __has_active_component: (component_type) =>
        if not @has_component(component_type) then return false
        components = @get_components(component_type)
        for _, component in pairs components do
            if component.__active then return true

        false

    destroy: =>
        @world\__mark_entity_for_destroy @

    activate: =>
        if not @active
            @world\__mark_entity_for_activation @

    __activate: =>
        if not @active then
            @active = true

            for _, component in pairs @components do
                if not component.__active then
                    component\activate!

            for _, engine in pairs @tracked_by do
                engine\__activate_entity @

    deactivate: =>
        if @active
            @world\__mark_entity_for_deactivation @

    __deactivate: =>
        if @active
            @active = false

            for _, component in pairs @components do
                if component.__active then
                    component\deactivate!

            for _, engine in pairs @tracked_by do
                engine\__deactivate_entity @

return Entity