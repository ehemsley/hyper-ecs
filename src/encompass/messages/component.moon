up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

Component = require(up_one_folder .. 'component')
Message = require(up_one_folder .. 'message')

return Message\define("ComponentMessage", { component: Component })