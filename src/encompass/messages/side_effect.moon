up_one_folder = (...)\gsub('%.[^%.]+$', '')\gsub('%.[^%.]+$', '') .. '.'

Message = require(up_one_folder .. 'message')

return Message\define("SideEffectMessage")