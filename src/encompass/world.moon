current_folder = (...)\gsub('%.[^%.]+$', '') .. '.'

prof = require(current_folder .. 'lib.jprof')
utils = require(current_folder .. "utils")
DrawComponent = require(current_folder .. "draw_component")
Message = require(current_folder .. "message")
StateMessage = require(current_folder .. "messages.state")
Entity = require(current_folder .. "entity")
PooledEntity = require(current_folder .. "pooled_entity")
MessagePool = require(current_folder .. "message_pool")

class World
    new: =>
        @next_id_value = 1

        @entities = {}

        @detecters = {}
        @spawners = {}
        @modifiers = {}
        @renderers = {}
        @side_effecters = {}

        @messages = {}
        @message_map = {}
        @message_types = {}
        @message_type_to_pool = {}
        @message_to_entity = {}
        @removed_messages = {}

        @component_type_to_engine_map = {}
        @message_type_to_processor_map = {}

        @state_messages = {}
        @state_message_type_to_processors_map = {}

        @id_to_entity = {}
        @component_entity_id_map = {}

        @entities_with_added_components = {}
        @entities_with_removed_components = {}

        @marked_for_activation = {}
        @marked_for_deactivation = {}
        @marked_for_destroy = {}

        @draw_layers = {}
        @draw_order_changed = false
        @draw_order = {}
        @draw_component_type_to_renderer = {}

    create_entity: =>
        id = @__next_id!
        entity = Entity id, self

        @entities[entity] = entity
        @id_to_entity[entity.id] = entity
        entity



    process_message_field_args = (message_klass, message, key, value, ...) ->
        if key == nil then return

        assert message_klass.field_types[key] ~= nil, key .. " is not defined in " .. message_klass.__name

        if value ~= nil
            assert utils.encompasstype(value, message_klass.field_types[key]),
                "value for '" ..
                key ..
                "' on " ..
                message_klass.__name .. " should be of type " .. tostring(message_klass.field_types[key])

            message[key] = value

        process_message_field_args message_klass, message, ...

    create_message: (message_klass, key, value, ...) =>
        assert message_klass ~= nil, "Message class is nil. Did you forget to require?"
        assert message_klass\subtype_of Message, message_klass.__name .. " is not a Message subclass"

        message_pool = @message_type_to_pool[message_klass]
        if message_pool == nil
            message_pool = MessagePool @, message_klass
            @message_type_to_pool[message_klass] = message_pool

        message = message_pool\obtain!

        if not utils.empty message_klass.required_field_types
            if key == nil or value == nil
                error "Missing arguments on " .. message_klass.__name

        process_message_field_args message_klass, message, key, value, ...

        for k, _ in pairs message_klass.required_field_types
            assert message[k] ~= nil, "missing field '" .. k .. "' on " .. message_klass.__name

        if @message_map[message_klass] == nil
            @message_map[message_klass] = {}
        table.insert @message_map[message_klass], message

        if @message_types[message_klass] == nil
            @message_types[message_klass] = {}
        table.insert @message_types[message_klass], message_klass

        if message\subtype_of StateMessage
            @state_messages[message] = message
        else
            @messages[message] = message

        referenced_entity = message.entity or utils.lookup_chain message, "component", "__entity"
        if referenced_entity
            @message_to_entity[message] = referenced_entity

        message

    add_detecter: (detecter_klass) =>
        detecter = detecter_klass @
        table.insert @detecters, detecter
        @__add_component_engine_relation detecter
        detecter

    add_spawner: (spawner_klass, ...) =>
        spawner = spawner_klass @, ...
        table.insert @spawners, spawner
        @__add_message_processor_relation spawner
        spawner

    add_modifier: (modifier_klass, ...) =>
        modifier = modifier_klass @, ...
        table.insert @modifiers, modifier
        @__add_message_processor_relation modifier
        modifier

    add_renderer: (renderer_klass) =>
        renderer = renderer_klass @
        table.insert @renderers, renderer
        @__add_component_engine_relation renderer
        for _, component_type in pairs renderer_klass.component_types
            if component_type\subtype_of DrawComponent
                @__register_draw_component_type_renderer_relation renderer, component_type
        renderer

    add_side_effecter: (side_effecter_klass, ...) =>
        side_effecter = side_effecter_klass @, ...
        table.insert @side_effecters, side_effecter
        @__add_message_processor_relation side_effecter
        side_effecter

    destroy_all_entities: =>
        for _, entity in pairs @entities
            entity\destroy!
        @__destroy_marked_entities!

    update: (dt) =>
        prof.push "frame"
        prof.push "World:update"

        prof.push "World:__check_entities_with_added_components"
        @__check_entities_with_added_components!
        prof.pop "World:__check_entities_with_added_components"

        prof.push "World:__check_entities_with_removed_components"
        @__check_entities_with_removed_components!
        prof.pop "World:__check_entities_with_removed_components"

        prof.push "World:__check_and_deactivate_entities"
        @__check_and_deactivate_entities!
        prof.pop "World:__check_and_deactivate_entities"

        prof.push "World:__check_and_activate_entities"
        @__check_and_activate_entities!
        prof.pop "World:__check_and_activate_entities"

        prof.push "World:__detect"
        @__detect!
        prof.pop "World:__detect"

        prof.push "World:__check_messages"
        @__check_messages!
        prof.pop "World:__check_messages"

        prof.push "World:__check_state_messages"
        @__check_state_messages!
        prof.pop "World:__check_state_messages"

        prof.push "World:__spawn"
        @__spawn!
        prof.pop "World:__spawn"

        prof.push "World:__untrack_respawned_entities"
        @__untrack_respawned_entities!
        prof.pop "World:__untrack_respawned_entities"

        prof.push "World:__modify"
        @__modify dt
        prof.pop "World:__modify"

        prof.push "World:__destroy_marked_entities"
        @__destroy_marked_entities!
        prof.pop "World:__destroy_marked_entities"

        if @draw_order_changed
            prof.push "World:__recalculate_draw_order"
            @__recalculate_draw_order!
            prof.pop "World:__recalculate_draw_order"
            @draw_order_changed = false

        prof.push "World:__side_effects"
        @__side_effects dt
        prof.pop "World:__side_effects"

        prof.push "World:__destroy_messages"
        @__destroy_messages!
        prof.pop "World:__destroy_messages"

        prof.pop "World:update"

    draw: (canvas) =>
        prof.push "World:draw"

        for _, component in pairs @draw_order
            renderer = @draw_component_type_to_renderer[component.__class]
            entity = component.__entity
            if renderer.tracked_entities[component.__entity] ~= nil
                prof.push(renderer.__class.__name .. ":render")
                renderer\render entity, canvas
                prof.pop(renderer.__class.__name .. ":render")

        prof.pop "World:draw"
        prof.pop "frame"

    find_entity: (id) =>
        @id_to_entity[id]

    -- FRAMEWORK-INTERNAL FUNCTIONS
    __next_id: =>
        id = @next_id_value
        @next_id_value += 1
        id

    __create_pooled_entity: (spawner) =>
        id = @__next_id!
        entity = PooledEntity id, self, spawner

        @entities[entity] = entity
        @id_to_entity[entity.id] = entity
        entity

    __detect: =>
        for _, detecter in pairs @detecters
            prof.push(detecter.__class.__name .. ":detect")
            detecter\__update!
            prof.pop(detecter.__class.__name .. ":detect")

    __spawn: =>
        for _, spawner in pairs @spawners
            prof.push(spawner.__class.__name .. ":spawn")
            spawner\__update!
            prof.pop(spawner.__class.__name .. ":spawn")
            spawner\__clean!

    __modify: (dt) =>
        for _, modifier in pairs @modifiers
            prof.push(modifier.__class.__name .. ":modify")
            modifier\__update dt
            prof.pop(modifier.__class.__name .. ":modify")
            modifier\__clean!

    __side_effects: (dt) =>
        for _, side_effecter in pairs @side_effecters
            prof.push(side_effecter.__class.__name .. ":effect")
            side_effecter\__update dt
            prof.pop(side_effecter.__class.__name .. ":effect")
            side_effecter\__clean!

    __check_entities_with_added_components: =>
        for k, entity in pairs @entities_with_added_components
            @__check_and_register_entity entity
            @entities_with_added_components[k] = nil

    __check_entities_with_removed_components: =>
        for k, entity in pairs @entities_with_removed_components
            for _, engine in pairs entity.tracked_by
                engine\__check_and_untrack_entity entity
            @entities_with_removed_components[k] = nil

    __check_and_deactivate_entities: =>
        for k, entity in pairs @marked_for_deactivation
            entity\__deactivate!
            @marked_for_deactivation[k] = nil

    __check_and_activate_entities: =>
        for k, entity in pairs @marked_for_activation
            entity\__activate!
            @marked_for_activation[k] = nil

    __create_component: (component_klass, entity) =>
        component = component_klass(entity)
        if @component_entity_id_map[component_klass] == nil
            @component_entity_id_map[component_klass] = {}

        @component_entity_id_map[component_klass][entity.id] = entity.id
        component

    __add_message_processor_relation: (processor) =>
        @message_type_to_processor_map[processor.__class.message_type] = processor

        if processor.__class.state_message_types ~= nil
            for _, state_component_type in pairs processor.__class.state_message_types
                if @state_message_type_to_processors_map[state_component_type] == nil
                    @state_message_type_to_processors_map[state_component_type] = {}
                table.insert @state_message_type_to_processors_map[state_component_type], processor

    __add_component_engine_relation: (engine) =>
        for _, component_type in pairs engine.__class.component_types
            if @component_type_to_engine_map[component_type] == nil
                @component_type_to_engine_map[component_type] = {}
            table.insert @component_type_to_engine_map[component_type], engine

    __register_draw_component_type_renderer_relation: (renderer, draw_component_klass) =>
        @draw_component_type_to_renderer[draw_component_klass] = renderer

    __deregister_draw_component_type_renderer_relation: (renderer, draw_component_klass) =>
        @draw_component_type_to_renderer[draw_component_klass] = nil

    __remove_draw_component: (draw_component) =>
        @draw_layers[draw_component.layer][draw_component] = nil
        @draw_order_changed = true

    __adjust_component_draw_layers: (draw_component, old_layer, new_layer) =>
        if @draw_layers[old_layer] ~= nil
            @draw_layers[old_layer][draw_component] = nil

        if @draw_layers[new_layer] == nil
            @draw_layers[new_layer] = {}

        @draw_layers[new_layer][draw_component] = draw_component
        @draw_order_changed = true

    __clear_message: (message) =>
        @removed_messages[message] = message
        @messages[message] = nil
        utils.remove @message_types[message.__class], message.__class
        utils.remove @message_map[message.__class], message

    __destroy_messages: =>
        for k, message in pairs @messages
            message\__deactivate!
            @messages[k] = nil

        for k, state_message in pairs @state_messages
            state_message\__deactivate!
            @state_messages[k] = nil

        utils.clear @removed_messages
        utils.clear @message_to_entity

    __clear_messages_for_entity: (given_entity) =>
        for message, entity in pairs @message_to_entity
            if entity == given_entity
                @__clear_message message

    -- TODO: refactor this so state messages have their own table
    __check_messages: =>
        for _, message in pairs @messages
            if @message_type_to_processor_map[message.__class] ~= nil
                processor = @message_type_to_processor_map[message.__class]
                processor\check_and_track_message message

    __check_state_messages: =>
        for _, state_message in pairs @state_messages
            if @state_message_type_to_processors_map[state_message.__class] ~= nil
                for __, processor in pairs @state_message_type_to_processors_map[state_message.__class]
                    processor\__check_and_track_state_message state_message

    __untrack_respawned_entities: =>
        for _, message in pairs @removed_messages
            if self.message_type_to_processor_map[message.__class] ~= nil
                processor = @message_type_to_processor_map[message.__class]
                processor\__untrack_message message

    __recalculate_draw_order: =>
        utils.clear @draw_order
        for _, components in utils.ordered_pairs @draw_layers
            for __, component in pairs components
                table.insert @draw_order, component

    __check_and_register_entity: (entity) =>
        for _, component in pairs entity.components
            if @component_type_to_engine_map[component.__class] ~= nil
                for __, engine in pairs @component_type_to_engine_map[component.__class]
                    engine\__check_and_track_entity entity

    __mark_entity_for_activation: (entity) =>
        @marked_for_activation[entity] = entity

    __mark_entity_for_deactivation: (entity) =>
        @marked_for_deactivation[entity] = entity

    __mark_entity_for_destroy: (entity) =>
        @marked_for_destroy[entity] = entity

    __destroy_marked_entities: =>
        for k, entity in pairs @marked_for_destroy
            @__destroy_entity entity
            @marked_for_destroy[k] = nil

    __destroy_entity: (entity) =>
        for _, engine in pairs entity.tracked_by
            engine\__untrack_entity entity

        for _, component in pairs entity.components
            entity\remove_component component

        @entities[entity] = nil
        @id_to_entity[entity.id] = nil
        @entities_with_added_components[entity] = nil
        @entities_with_removed_components[entity] = nil

return World